package io.matthd.pyrexdust;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matthew on 2016-02-14.
 */
public class PyrexDust extends JavaPlugin {

    private static PyrexDust instance;
    DustEnchant ench = new DustEnchant(71);

    public void onEnable() {
        instance = this;
        getCommand("dust").setExecutor(new DustCommand());

        Bukkit.getServer().getPluginManager().registerEvents(new DustListener(), this);

        registerEnchant();
    }

    public void onDisable() {
        instance = null;
    }

    public boolean isProtectedItem(ItemStack item) {
        ItemMeta meta = item.getItemMeta();

        if (meta.getLore() == null) {
            return false;
        } else {

            List<String> lore = meta.getLore();

            return lore.contains(ChatColor.WHITE +ChatColor.BOLD.toString()+ "Protected");
        }
    }

    public ItemStack protectItem(ItemStack item) {
        ItemMeta meta = item.getItemMeta();
        item.addUnsafeEnchantment(ench, 1);
        meta.addEnchant(ench, 1, true);

        List<String> lore;

        if(meta.getLore() == null){
            lore = new ArrayList<>();
        }
        else {
            lore = meta.getLore();
        }

        lore.add(ChatColor.WHITE + ChatColor.BOLD.toString() + "Protected");
        meta.setLore(lore);
        item.setItemMeta(meta);

        return item;
    }

    public ItemStack unprotectItem(ItemStack item) {
        item.removeEnchantment(ench);
        ItemMeta meta = item.getItemMeta();

        List<String> lore = meta.getLore();
        lore.stream().filter(s -> s.equalsIgnoreCase(ChatColor.WHITE + ChatColor.BOLD.toString() + "Protected")).forEach(lore::remove);
        meta.setLore(lore);
        item.setItemMeta(meta);
        return item;
    }

    private void registerEnchant() {

        try {
            try {
                Field f = Enchantment.class.getDeclaredField("acceptingNew");
                f.setAccessible(true);
                f.set(null, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Enchantment.registerEnchantment(ench);
            } catch (IllegalArgumentException e) {
//if this is thrown it means the id is already taken.
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static PyrexDust getInstance() {
        return instance;
    }
}
