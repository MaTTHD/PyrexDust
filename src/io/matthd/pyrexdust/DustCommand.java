package io.matthd.pyrexdust;

import net.minecraft.server.v1_8_R3.Item;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftItem;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Matthew on 2016-02-14.
 */
public class DustCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if(cmd.getName().equalsIgnoreCase("dust")){
            if(args.length == 3){
                if(args[0].equalsIgnoreCase("give")){
                    if(!sender.hasPermission("pyrex.dust.give")){
                        sender.sendMessage(ChatColor.RED + "You do not have permission to use this command!");
                        return true;
                    }

                    int amount = 0;

                    Player target = Bukkit.getPlayer(args[1]);
                    if(target == null){
                        sender.sendMessage(ChatColor.RED + "That player is not online!");
                        return true;
                    }

                    try {
                        amount = Integer.parseInt(args[2]);
                    } catch (NumberFormatException e){
                        sender.sendMessage(ChatColor.RED + "The amount must be a number!");
                    }

                    ItemStack item = new ItemStack(Material.SUGAR, amount);
                    ItemMeta meta = item.getItemMeta();

                    meta.setDisplayName(ChatColor.DARK_PURPLE + ChatColor.BOLD.toString() + "Protect Dust");
                    item.setItemMeta(meta);

                    target.getInventory().addItem(item);
                    sender.sendMessage(ChatColor.GREEN + "Gave " + amount + " magic dust too " + target.getName());
                    return true;
                }
            }
            else {
                sender.sendMessage(ChatColor.RED + "/dust give [name] [amount]");
            }
        }

        return false;
    }
}
