package io.matthd.pyrexdust;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Created by Matthew on 2016-02-14.
 */
public class DustListener implements Listener {

    private PyrexDust instance = PyrexDust.getInstance();

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        ItemStack itemOnHand = e.getCursor();
        ItemStack toApply = e.getCurrentItem();

        if (itemOnHand == null || toApply == null || itemOnHand.getType() == Material.AIR || toApply.getType() == Material.AIR) {
            return;
        }

        ItemMeta dustMeta = itemOnHand.getItemMeta();

        if (!dustMeta.getDisplayName().equalsIgnoreCase(ChatColor.DARK_PURPLE + ChatColor.BOLD.toString() + "Protect Dust")) {
            return;
        }

        if (e.isLeftClick()) {
            if (!instance.isProtectedItem(toApply)) {
                e.setCancelled(true);
                itemOnHand.setType(Material.AIR);
                e.getCursor().setType(Material.AIR);
                ((Player) e.getWhoClicked()).updateInventory();
                e.getInventory().setItem(e.getSlot(), instance.protectItem(toApply));
                ((Player) e.getWhoClicked()).updateInventory();
            } else {
                e.setCancelled(true);
                e.getWhoClicked().sendMessage(ChatColor.RED + "You have already protected this item!");
                ((Player) e.getWhoClicked()).closeInventory();

                return;
            }
        } else {
            return;
        }
    }
}
