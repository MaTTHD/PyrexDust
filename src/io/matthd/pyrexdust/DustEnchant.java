package io.matthd.pyrexdust;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Matthew on 2016-02-14.
 */
public class DustEnchant extends Enchantment {
    public DustEnchant(int id) {
        super(id);
    }

    @Override
    public String getName() {
        return "Protect Dust";
    }

    @Override
    public int getMaxLevel() {
        return 1;
    }

    @Override
    public int getStartLevel() {
        return 1;
    }

    @Override
    public EnchantmentTarget getItemTarget() {
        return null;
    }

    @Override
    public boolean conflictsWith(Enchantment enchantment) {
        return false;
    }

    @Override
    public boolean canEnchantItem(ItemStack itemStack) {
        return true;
    }
    @Override
    public int getId(){
        return 71;//the id, in this case I choose 69 because no serious person would ever choose that number, lucky me! Oh yeah, the max id = 256
    }
}
